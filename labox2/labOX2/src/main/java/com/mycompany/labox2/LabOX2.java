/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.labox2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class LabOX2 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'X';
    static int row, col;
    static String yn;
    static boolean True = true;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();
            if (isWin()) {
                printTable();
                printWin();
                inputContinue();
                break;
            }
            if (checkDraw()) {
                printTable();
                System.out.println("Draw");
                inputContinue();
                break;

            }
            changePlayer();
        }
    }

    private static void printWelcome() {
        System.out.println("Welcome to XO");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println(currentPlayer + " Turn");
    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row,col:");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }
        }
    }

    private static void changePlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWin() {
        if (checkRow()) {
            return true;
        } else if (checkCol()) {
            return true;
        } else if (checkDg()) {
            return true;
        } else if (checkDg2()) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println(currentPlayer + " Winner");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int j = 0; j < 3; j++) {
            if (table[j][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDg() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDg2() {
        for (int i = 0; i < 3; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    
        private static void EndGame() {
        System.out.println("EndGame");
    }

    private static void inputContinue() {
        System.out.println("Continue (y/n)");
        Scanner kb = new Scanner(System.in);
        yn = kb.next();
        if (yn.equals("y")) {
            resetTable();
            Restart();
        } else if (yn.equals("n")) {
            EndGame();
            True = false;
        }
    }

    private static void resetTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '-';
            }

        }
    }

    private static void Restart() {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();
            if (isWin()) {
                printTable();
                printWin();
                inputContinue();
                break;
            }
            if (checkDraw()) {
                printTable();
                System.out.println("Draw");
                inputContinue();
                break;

            }
            changePlayer();
        }
    }
}
